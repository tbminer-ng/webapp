from fastapi import FastAPI, Response, status, HTTPException
from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)



client = TestClient(app)
def test_base():
    response = client.get("./")
    print(response)
    assert response.status_code == 200

def test_static():
    response = client.get("./static")
    print(response)
    assert response.status_code == 200

def test_index():
    response = client.get("./static/index.html")
    print(response)
    assert response.status_code == 200

def test_data_sample():
    response = client.get("./api/data/sample")
    assert response.status_code == 200

def test_data_unlabel_data():
    response = client.get("./api/data/unlabel_data")
    assert response.status_code == 200

def test_front_inputdescription():
    response = client.get("./api/front/inputdescription")
    assert response.status_code == 200

def test_front_contact():
    response = client.get("./api/front/contact")
    assert response.status_code == 200

def test_front_outputdescription():
    response = client.get("./api/front/outputdescription")
    assert response.status_code == 200

def test_front_otherdescription():
    response = client.get("./api/front/otherdescription")
    assert response.status_code == 200

def test_upload_file():
    test_file = 'data/sample_vntr_sp.csv'
    files = {'file': ('sample_vntr_sp.csv', open(test_file, 'rb'))}
    response = client.post('/api/predict/csv2json', files=files, params={'model_input': "vntr_spacer"})
    assert response.status_code == status.HTTP_202_ACCEPTED

def test_upload_file_csv():
    test_file = 'data/sample_vntr_sp.csv'
    files = {'file': ('sample_vntr_sp.csv', open(test_file, 'rb'))}
    response = client.post('/api/predict/csv2json', files=files,  params={'return_type': 'csv', 'model_input': "vntr_spacer"})
    assert response.status_code == status.HTTP_202_ACCEPTED

def test_upload_file_csv_error_input():
    test_file = 'data/sample_vntr_sp.csv'
    files = {'file': ('sample_vntr_sp.csv', open(test_file, 'rb'))}
    response = client.post('/api/predict/csv2json', files=files,  params={'return_type': 'csv', 'model_input': "invalid_inputr"})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
def test_models_score():
    response = client.get('/models_score')
    assert response.status_code == 200