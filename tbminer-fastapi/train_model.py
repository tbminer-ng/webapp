import logging
import warnings
import numpy as np
import pandas as pd
import skops.io as sio
import xgboost as xgb
import yaml
from imblearn.over_sampling import RandomOverSampler
from sklearn.base import clone
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, LabelEncoder

warnings.filterwarnings('ignore')
from sklearn.metrics import f1_score, accuracy_score
import numpy as np

test_size=0.2
validation_size=0.5

def f1_eval(y_pred, dtrain):
    y_train_list = []
    for dlist in dtrain:
        y_train_list = y_train_list + [np.argmax(dlist)]
    err = 1-f1_score(y_pred, y_train_list, average='macro')
    return err

def fit_and_score(estimator, X_train, X_test, y_train, y_test):
    """Fit the estimator on the train set and score it on both sets"""
    estimator.fit(X_train, y_train, eval_set=[(X_train, y_train),(X_test, y_test)],verbose=1)
    
    X_train = X_train
    y_test = y_test

    train_score = estimator.score(X_train, y_train)
    test_score = estimator.score(X_test, y_test)
    y_predict = estimator.predict(X_test)
    y_true = y_test
    learning = estimator.evals_result()
    return estimator, train_score, test_score, y_predict, y_true, learning

#def fit_and_score_train(estimator, X_train, y_train):
#    """Fit the estimator on the train set and score it on both sets"""
#    #estimator.fit(X_train, y_train, eval_set=[(X_train, y_train),(X_test, y_test)],verbose=1)
#    estimator.fit(X_train, y_train, eval_set=[(X_train, y_train)],verbose=10000000)


def train_models( test_size=0.2, ):
    with open("./model_config/data_input_columns.yaml", "r") as data_input_columns_file:
        data_input = yaml.load(data_input_columns_file, Loader=yaml.FullLoader)

    with open("./model_config/data_output_columns.yaml", "r") as data_output_file:
        data_output = yaml.load(data_output_file, Loader=yaml.FullLoader)

    with open("./model_config/resample_output_class.yaml", "r") as resample_output_file:
        resample_dict = yaml.load(resample_output_file, Loader=yaml.FullLoader)
        logging.info(resample_dict)

    print(data_output.keys())
    print(data_input.keys())

    wrongLabels = ['Unknow', 'Unknown']

    with open("./model_config/clf_best_param.yaml", "r") as clf_best_param_file:
        clf_param_dict = yaml.load(clf_best_param_file, Loader=yaml.FullLoader)

    model_path = './models'
    best_model = 'XGBClassifier'

    result = {}

    for input_data in clf_param_dict:
        result[input_data] = {}
        for output_model in clf_param_dict[input_data]:
            print(output_model)
            result[input_data][output_model] = {}

            print(" {0} {1}".format(input_data, output_model))
            early_stopping_rounds = clf_param_dict[input_data][output_model]['early_stopping_rounds']
            max_depth = clf_param_dict[input_data][output_model]['max_depth']
            subsample = clf_param_dict[input_data][output_model]['subsample']
            n_estimators = clf_param_dict[input_data][output_model]['n_estimator']

            df_all = pd.read_csv("./data/mtbc_clean.csv", sep=";")
            label_data = df_all[[data_output[output_model]] + data_input[input_data]]

            label_data = label_data[~label_data[data_output[output_model]].isin(wrongLabels)]

            label_data['barcode'] = label_data[data_input[input_data]]\
                .astype('str')\
                .apply(lambda x: 'x'.join(x), axis=1)
            
            df_res = label_data[['barcode'] + [data_output[output_model]]]\
                .groupby(by='barcode', group_keys=True)\
                .count()
            
            df_res.sort_values(data_output[output_model], ascending=False)

            df_res['label'] = label_data[['barcode'] + [data_output[output_model]]]\
                .groupby(by='barcode', group_keys=True)\
                .agg(lambda x: list(set(x)))
            
            df_res['duplicate'] = df_res['label'].str.len()
            df_res = df_res.sort_values(data_output[output_model], ascending=False)
            df_duplicate = df_res[df_res.duplicate > 1].reset_index()

            # print("duplicate ind with different classification")
            # print(pd.DataFrame(df_duplicate[data_output[output_model]].value_counts()))

            df_wduplicate = label_data.astype('str')
            df_wduplicate = df_wduplicate[~df_wduplicate['barcode'].isin(df_duplicate['barcode'])]
            df_wduplicate = df_wduplicate.drop(['barcode'], axis=1)
            df_wduplicate[data_output[output_model]].value_counts()

            ros = RandomOverSampler(random_state=42, sampling_strategy=resample_dict[output_model])

            X_wduplicate_oversample, y_wduplicate_oversample = ros.fit_resample(
                df_wduplicate.loc[:, df_wduplicate.columns != data_output[output_model]],
                df_wduplicate.loc[:, df_wduplicate.columns == data_output[output_model]])

            df_wduplicate_oversample = pd.concat([X_wduplicate_oversample, y_wduplicate_oversample], axis=1)

            df_low_oversample = pd.DataFrame(df_wduplicate_oversample[data_output[output_model]].value_counts())

            low_class_oversample = list(df_low_oversample[df_low_oversample[data_output[output_model]] < 11].index)

            # print("Low class remove from dataset {}".format(low_class_oversample))

            df_wduplicate_wlowclass = df_wduplicate_oversample[
                ~df_wduplicate_oversample[data_output[output_model]].isin(low_class_oversample)]



            for i in range(20):
                df_wduplicate_wlowclass = df_wduplicate_wlowclass.append(
                    pd.Series('missing', index=df_wduplicate_wlowclass.columns), ignore_index=True)

            df_train, df_test = train_test_split(df_wduplicate_wlowclass,
                                                 test_size=test_size,
                                                 random_state=42,
                                                 stratify=df_wduplicate_wlowclass[
                                                     data_output[output_model]].apply(str.title),
                                                 )




            # df_train_1[data_output[output_model]].value_counts()
            # df_test[data_output[output_model]].value_counts().shape

            # df_test, df_validation = train_test_split( df_test_validation,
            #                                     test_size=validation_size,
            #                                     random_state=43,
            #                                     stratify=df_test_validation[data_output[output_model]].apply(str.title),
            #                                    )

            # TRAIN DATA
            ## Cast to int and train ONeHOtEncoder for X train
            X_train = df_train[data_input[input_data]]\
                .replace("?", np.nan) \
                .replace("missing", np.nan) \
                .apply(pd.to_numeric, errors='coerce', downcast='integer').fillna(np.nan)

            print(X_train.tail(10))

            X_category = X_train.dropna().astype(int)
            encoder = OneHotEncoder(handle_unknown="ignore", sparse_output=False)
            encoder.fit(X_category)

            ## train LabelEncoder with y train data
            y_train = df_train[data_output[output_model]]
            label_encoder = LabelEncoder()
            label_encoder.fit(y_train)

            ## Encode (X,y) train data
            X_train_encode = encoder.transform(X_train)
            y_train_encode = label_encoder.transform(y_train)

            # VALIDATION DATA
            ## Split (X,y) from df and cast to int X validation data
            # X_validation = df_validation[data_input[input_data]]\
            #    .replace("?", np.nan)\
            #    .apply(pd.to_numeric,
            #           errors='coerce',
            #           downcast='integer')\
            #    .fillna(np.nan)
            # y_validation = df_validation[data_output[output_model]]

            ## Encode (X,y) validation data
            # X_validation_encode = encoder.transform(X_validation)
            # y_validation_encode = label_encoder.transform(y_validation)

            # TEST DATA
            ## Split (X,y) from df and cast to int X validation data
            X_test = df_test[data_input[input_data]] \
                .replace("?", np.nan) \
                .replace("missing", np.nan) \
                .apply(pd.to_numeric,
                       errors='coerce',
                       downcast='integer') \
                .fillna(np.nan)
            y_test = df_test[data_output[output_model]]

            ## Encode (X,y) test data
            X_test_encode = encoder.transform(X_test)
            y_test_encode = label_encoder.transform(y_test)

            #######
            clf = xgb.XGBClassifier(n_estimators=n_estimators + 100,
                                    tree_method="hist",
                                    early_stopping_rounds=early_stopping_rounds,
                                    max_depth=max_depth,
                                    subsample=subsample,
                                    eval_metric=f1_eval, verbosity=1,
                                    )


            # split dataset based on CV
            X_train = X_train_encode
            # X_val = X_validation_encode
            y_train = y_train_encode
            # y_val = y_validation_encode

            # run training
            est, train_score, val_score, y_validation_predict, y_validation_true, learning = fit_and_score(
                clone(clf), X_train, X_test_encode, y_train, y_test_encode
            )
            # est, train_score, learning = fit_and_score_train(clone(clf), X_train, y_train)
            #############

            # y_validation_true = y_val
            # y_validation_predict = est.predict(X_val)

            print()
            print("###########################################################")
            print("###########################################################")
            print()
            print(" {0} {1}".format(input_data, output_model))
            # print("Validation Accuracy : {0}".format(val_score))
            # rint("Validation f1_score  : {0}".format(np.round(f1_score(y_validation_true, y_validation_predict, average='macro'), 3)))

            y_test_predict_encode = est.predict(X_test_encode)
            print("Test best f1_score iteration : {0} ".format(est.best_iteration))
            print("Test f1_score : {0}".format(
                np.round(f1_score(y_test_encode, y_test_predict_encode, average='macro'), 3)))
            print("Model validation f1_score : {0}".format(
                np.round(clf_param_dict[input_data][output_model]['f1_macro_mean'], 3)))

            # result[input_data][output_model]["Validation Accuracy"] = val_score
            # result[input_data][output_model]["Validation f1_score"] = np.round(f1_score(y_validation_true, y_validation_predict, average='macro'), 3)
            result[input_data][output_model]["Test f1_score"] = np.round(
                f1_score(y_test_encode, y_test_predict_encode, average='macro'), 3)

            result[input_data][output_model]["Test accuracy"] = np.round(
                accuracy_score(y_test_encode, y_test_predict_encode), 3)

            result[input_data][output_model]["Model validation f1_score"] = np.round(
                clf_param_dict[input_data][output_model]['f1_macro_mean'], 3)

            print()
            print("###########################################################")
            print("###########################################################")
            print()
            # print(classification_report(label_encoder.inverse_transform(y_true),
            #                            label_encoder.inverse_transform(y_predict)))

            model_file_json = "{0}/{1}_{2}_{3}_model.json".format(
                model_path,
                best_model,
                input_data,
                output_model
            )
            model_file = "{0}/{1}_{2}_{3}_model.skops".format(
                model_path,
                best_model,
                input_data,
                output_model
            )
            labelencoder_file = "{0}/{1}_{2}_{3}_labelencoder.skops".format(
                model_path,
                best_model,
                input_data,
                output_model
            )
            onehotencoder_file = "{0}/{1}_{2}_{3}_onehotencoder.skops".format(
                model_path,
                best_model,
                input_data,
                output_model
            )

            # sio.dump(est, model_file)
            est.save_model(model_file_json)

            sio.dump(label_encoder, labelencoder_file)
            sio.dump(encoder, onehotencoder_file)

    print(pd.DataFrame(result).T.stack().to_frame()[0].apply(pd.Series))
    print(result)
    print("n_estimators : {0}".format(n_estimators))
    pd.DataFrame(result).T.stack().to_frame()[0].apply(pd.Series).to_csv('{0}/models_score.csv'.format(
                model_path))


    return est, encoder, label_encoder, X_test, y_test



if __name__ == "__main__":
    train_models()