## Import
import os
import logging
import os
import uuid
import json
import uvicorn
import gunicorn
#import pytest
import httpx
import numpy
import numpy as np
import pandas as pd
import skops.io as sio
import yaml
from fastapi import FastAPI, Response, status, HTTPException
from fastapi import Query, File, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from sklearn.impute import SimpleImputer
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from sklearn.semi_supervised import SelfTrainingClassifier
from starlette.responses import StreamingResponse
from starlette.testclient import TestClient
from xgboost import XGBClassifier
import markdown
import xgboost as xgb
from fastapi.responses import FileResponse



## Configuration file
with open("config.yaml", "r") as config_file:
    config = yaml.load(config_file, Loader=yaml.FullLoader)

with open("frontend/contact.yaml", "r") as contact_file:
    contact = yaml.load(contact_file, Loader=yaml.FullLoader)

with open("frontend/inputdescription.yaml", "r") as inputdescription_file:
    inputdescription = yaml.load(inputdescription_file, Loader=yaml.FullLoader)

with open("frontend/outputdescription.yaml", "r") as outputdescription_file:
    outputdescription = yaml.load(outputdescription_file, Loader=yaml.FullLoader)

with open("frontend/otherdescription.yaml", "r") as otherdescription_file:
    otherdescription = yaml.load(otherdescription_file, Loader=yaml.FullLoader)

with open("model_config/predict_matrix.yaml", "r") as predict_matrix_file:
    predict_matrix = yaml.load(predict_matrix_file, Loader=yaml.FullLoader)

with open("model_config/data_input_columns.yaml", "r") as data_input_columns_file:
    data_input = yaml.load(data_input_columns_file, Loader=yaml.FullLoader)

with open("model_config/data_output_columns.yaml", "r") as data_output_file:
    data_output = yaml.load(data_output_file, Loader=yaml.FullLoader)


## Fastapi configuration
base_path = os.getenv('BASE_PATH', config['base_path'])
FORMAT = "%(levelname)s:%(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)

app_root = FastAPI()
app = FastAPI()

app_root.mount(base_path, app)

app.mount("/static/", StaticFiles(directory="static",html = True), name="static")


#app.add_middleware(
#    CORSMiddleware,
#    allow_origins=["*"],
#    allow_credentials=True,
#    allow_methods=["*"],
#    allow_headers=["*"],
#)



sep_list = [",", ";","\t" " "]
vntr = data_input['vntr']
vntr15 = data_input['vntr15']
spacer = data_input['spacer']
spacer_vntr = data_input['vntr_spacer']
model_input_list = list(data_input.keys())
model_output_list = list(data_output.keys())


## REST API funcion
@app_root.get("/", response_class=RedirectResponse)
async def redirect_fastapi():
    return config['base_path']

@app.get("/", response_class=RedirectResponse)
async def redirect_fastapi():
    return "./static/"

@app.get("/models_score", response_class=FileResponse)
async def read_data():
    file_path = "models/models_score.csv"
    df = pd.read_csv(file_path)
    return Response(df.to_json(orient="records"), media_type="application/json")

@app.get("/api/data/sample")
def get_data_sample():
    df_train = pd.read_csv("data/mtbc_clean.csv", sep=";")
    df_sample = df_train[vntr + spacer]
    df_sample.reset_index()
    _df_,df_sample_subset = train_test_split(df_sample, test_size=50, random_state=42)
    return StreamingResponse(
        iter([df_sample_subset.to_csv(index_label="ID")]),
        media_type="text/csv",
        headers={"Content-Disposition": f"attachment; filename=sample_vntr_sp.csv"}
    )

@app.get("/api/data/unlabel_data")
def data_all():
    absolute_path = os.getcwd()
    relative_path = "data/user"
    full_path = os.path.join(absolute_path, relative_path)
    all_files = os.listdir(os.path.join(full_path))
    csv_files = list(filter(lambda f: f.endswith('.csv'), os.listdir(relative_path)))
    li = []
    for filename in csv_files:
        print(full_path + "/" +filename)
        try:
            df = pd.read_csv(relative_path + "/" + filename, index_col=None, header=0)
        except Exception as e:
            print(e)
            # continue
        li.append(df)
    frame = pd.concat(li, axis=0, ignore_index=True)
    frame = frame.apply(pd.to_numeric,errors='coerce')
    frame = frame.fillna(np.nan)
    frame = frame.replace("?", np.nan)
    #spacer_vntr
    df_vntr = frame[spacer_vntr].dropna().astype(int).dropna().drop_duplicates().reset_index()
    return StreamingResponse(
        iter([df_vntr.to_csv(index=False)]),
        media_type="text/csv",
        headers={"Content-Disposition": f"attachment; filename=data.csv"}
    )

@app.get("/api/front/inputdescription")
def get_front_inputdescription():
    return JSONResponse(inputdescription)

@app.get("/api/front/contact")
def get_front_contact():
    return JSONResponse(contact)

@app.get("/api/front/outputdescription")
def get_front_outputdescription():
    return JSONResponse(outputdescription)

@app.get("/api/front/otherdescription")
def get_front_otherdescription():
    return JSONResponse(otherdescription)

@app.post("/api/predict/csv2json", status_code=status.HTTP_202_ACCEPTED)
def csv2json(file: UploadFile = File(...),
             model_input: str = Query(model_input_list[0], enum=model_input_list),
             sep: str = Query(sep_list[0], enum=sep_list),
             return_type: str = "json"):
    def f1_eval(y_pred, dtrain):
        y_train_list = []
        for dlist in dtrain:
            y_train_list = y_train_list + [np.argmax(dlist)]
        err = 1-np.round(f1_score(y_pred, y_train_list, average='macro'), 3)
        return err

    model_path = "models"
    best_model = "XGBClassifier"
    if model_input == "spacer":
        input_data = spacer
    elif (model_input == "vntr"):
        input_data = vntr
    elif (model_input == "vntr15"):
        input_data = vntr15
    elif (model_input == "vntr_spacer"):
        input_data = spacer_vntr
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="unknow separator")

    try:
        df_predict = pd.read_csv(file.file, sep=sep)
    except:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="error when load csv")

    logging.info(df_predict.columns)
    logging.info(df_predict.dtypes)

    df_predict.to_csv("data/user/{0}.csv".format(str(uuid.uuid4())))

    logging.info(model_input)
    logging.info(predict_matrix)
    logging.info(predict_matrix[model_input])
    outputs = predict_matrix[model_input]
    for output_model in outputs:
        model_file = "{0}/{1}_{2}_{3}_model.skops".format(
            model_path,
            best_model,
            model_input,
            output_model
        )
        model_file_json = "{0}/{1}_{2}_{3}_model.json".format(
            model_path,
            best_model,
            model_input,
            output_model
        )
        labelencoder_file = "{0}/{1}_{2}_{3}_labelencoder.skops".format(
            model_path,
            best_model,
            model_input,
            output_model
        )
        onehotencoder_file = "{0}/{1}_{2}_{3}_onehotencoder.skops".format(
            model_path,
            best_model,
            model_input,
            output_model
        )

        clf_load = xgb.XGBClassifier()
        clf_load.load_model(model_file_json)

        unknown_types_labelencoder = sio.get_untrusted_types(file=labelencoder_file)
        label_encoder_load = sio.load(file=labelencoder_file, trusted=unknown_types_labelencoder)
        unknown_types_onehotencoder = sio.get_untrusted_types(file=onehotencoder_file)
        encoder_load = sio.load(file=onehotencoder_file, trusted=unknown_types_onehotencoder)

        X_predict = df_predict[data_input[model_input]]\
            .replace("?", np.nan)\
            .apply(pd.to_numeric, errors='coerce', downcast='integer')\
            .fillna(np.nan)
        X_predict_encode = encoder_load.transform(X_predict)
        logging.info("################  X_predict #################")
        y_predict_encode = clf_load.predict(X_predict_encode)
        predict = label_encoder_load.inverse_transform(y_predict_encode)
        df_predict['predict_{0}'.format(output_model)] = list(predict)
        predict_proba = clf_load.predict_proba(X_predict_encode)
        df_predict['predict_proba_{0}'.format(output_model)] = list(np.amax(predict_proba, axis=1))
    logging.info(return_type)
    #df_predict.to_csv("data/user/{0}-predict.csv".format(str(uuid.uuid4())))
    if return_type == "json":
        return df_predict.to_json(orient="records")
    else:
        logging.info("CSV")
        return StreamingResponse(
            iter([df_predict.to_csv(index=False)]),
            media_type="text/csv",
            headers={"Content-Disposition": f"attachment; filename=data.csv"},
            status_code=status.HTTP_202_ACCEPTED
        )





if __name__ == "__main__":
    uvicorn.run(app_root, host="127.0.0.1", port=8000)