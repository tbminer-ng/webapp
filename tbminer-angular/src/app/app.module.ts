import {LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import * as fr from '@angular/common/locales/fr';
import {registerLocaleData} from "@angular/common";
import { HeaderComponent } from './component/header/header.component';
import {AppRoutingModule} from "./app-routing.module";
import { UploadToPredict } from './component/mainContent/upload-to-predict/upload-to-predict.component';
import {HttpClientModule} from "@angular/common/http";
import {MatTableModule} from "@angular/material/table";
import { MatTableExporterModule } from 'mat-table-exporter';
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatButtonModule} from "@angular/material/button";
import {MatSortModule} from "@angular/material/sort";
import {MatInputModule} from "@angular/material/input";
import {FormsModule} from "@angular/forms";

import {MatSidenavModule} from "@angular/material/sidenav";
import {MatDividerModule} from "@angular/material/divider";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatExpansionModule} from "@angular/material/expansion";
import { NavComponent } from './component/nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatListModule } from '@angular/material/list';
import {MatStepperModule} from "@angular/material/stepper";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";

import {MatDialogModule} from "@angular/material/dialog";
import {MatTabsModule} from "@angular/material/tabs";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatSelectModule} from "@angular/material/select";
import {MatRadioModule} from "@angular/material/radio";
import { NameCorrespondanceComponent } from './component/side/name-correspondance/name-correspondance.component';


import { DialogTemplateComponent } from './component/dialog-template/dialog-template.component';
import {MatGridListModule} from "@angular/material/grid-list";

import {MatMenuModule} from "@angular/material/menu";



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UploadToPredict,
    NavComponent,
    NameCorrespondanceComponent,
    DialogTemplateComponent,
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        MatTableModule,
        MatTableExporterModule,
        MatPaginatorModule,
        MatButtonModule,
        MatSortModule,
        MatInputModule,
        FormsModule,
        MatSidenavModule,
        MatDividerModule,
        MatToolbarModule,
        MatIconModule,
        MatTooltipModule,
        MatExpansionModule,
        LayoutModule,
        MatListModule,
        MatStepperModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatTabsModule,
        MatButtonToggleModule,
        MatSelectModule,
        MatRadioModule,
        MatGridListModule,
        MatMenuModule,
    ],

  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR'}
  ],
  bootstrap: [AppComponent],
  exports: [
    NavComponent
  ]
})
export class AppModule {
  constructor() {
    registerLocaleData(fr.default);
  }
}
