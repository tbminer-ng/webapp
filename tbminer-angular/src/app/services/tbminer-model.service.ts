import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TbminerModelService {
  sep= [",",";",'\t',' '];
  vntrRegex = /.*vntr.*([0-9]{4,4})(b{0,1}).*/i;
  spRegex = /.*sp\D*([0-9]+)\D*/i;
  miruRegex: {[miruKey: string]: RegExp } = {
    'VNTR_0154'	  : /.*MIRU.*0{0,1}2.*/i,
    'VNTR_0580'	  : /.*MIRU.*0{0,1}4.*|.*ETR.?D.*/i,
    'VNTR_0960'   :	/.*MIRU.*10.*/i,
    'VNTR_1644'	  : /.*MIRU.*16.*/i,
    'VNTR_2059'   :	/.*MIRU.*20.*/i,
    'VNTR_2531'   :	/.*MIRU.*23.*/i,
    'VNTR_2687'   :	/.*MIRU.*24.*/i,
    'VNTR_2996'	  :	/.*MIRU.*26.*/i,
    'VNTR_3007'	  :	/.*MIRU.*27.*|.*QUBS.*/i,
    'VNTR_3192'	  :	/.*MIRU.*31.*|.*ETR.?E.*/i,
    'VNTR_4348'	  :	/.*MIRU.*39.*/i,
    'VNTR_0802'	  :	/.*MIRU.*40.*/i,
    'VNTR_0424'	  :	/.*Mtub.*04.*/i,
    'VNTR_0577'	  :	/.*ETR.?C.*/i,
    'VNTR_1955'	  :	/.*Mtub.*21.*/i,
    'VNTR_2163b'	:	/.*QUB.*11b.*/i,
    'VNTR_2165'	  :	/.*ETR.?A.*/i,
    'VNTR_2347'	  :	/.*Mtub.*29.*/i,
    'VNTR_2401'	  :	/.*Mtub.*30.*/i,
    'VNTR_2461'	  :	/.*ETR.?B.*/i,
    'VNTR_3171'	  :	/.*Mtub.*34.*/i,
    'VNTR_3690'	  :	/.*Mtub.*39.*/i,
    'VNTR_4156'	  :	/.*QUB.*4156.*/i,
    'VNTR_4052'	  :	/.*QUB.*26.*/i,
  }
}
