import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class CsvHeaderService {
  csvHeader = ["VNTR_0580","VNTR_2996","VNTR_0802","VNTR_0960","VNTR_1644","VNTR_3192","VNTR_0424","VNTR_0577","VNTR_2165","VNTR_2401","VNTR_3690","VNTR_4156","VNTR_2163b","VNTR_1955","VNTR_4052","VNTR_0154","VNTR_2531","VNTR_4348","VNTR_2059","VNTR_2687","VNTR_3007","VNTR_2347","VNTR_2461","VNTR_3171","spoligo1","spoligo2","spoligo3","spoligo4","spoligo5","spoligo6","spoligo7","spoligo8","spoligo9","spoligo10","spoligo11","spoligo12","spoligo13","spoligo14","spoligo15","spoligo16","spoligo17","spoligo18","spoligo19","spoligo20","spoligo21","spoligo22","spoligo23","spoligo24","spoligo25","spoligo26","spoligo27","spoligo28","spoligo29","spoligo30","spoligo31","spoligo32","spoligo33","spoligo34","spoligo35","spoligo36","spoligo37","spoligo38","spoligo39","spoligo40","spoligo41","spoligo42","spoligo43"];
  vntrHeader = ["VNTR_0580","VNTR_2996","VNTR_0802","VNTR_0960","VNTR_1644","VNTR_3192","VNTR_0424","VNTR_0577","VNTR_2165","VNTR_2401","VNTR_3690","VNTR_4156","VNTR_2163b","VNTR_1955","VNTR_4052","VNTR_0154","VNTR_2531","VNTR_4348","VNTR_2059","VNTR_2687","VNTR_3007","VNTR_2347","VNTR_2461","VNTR_3171"];
  spHeader = ["spoligo1","spoligo2","spoligo3","spoligo4","spoligo5","spoligo6","spoligo7","spoligo8","spoligo9","spoligo10","spoligo11","spoligo12","spoligo13","spoligo14","spoligo15","spoligo16","spoligo17","spoligo18","spoligo19","spoligo20","spoligo21","spoligo22","spoligo23","spoligo24","spoligo25","spoligo26","spoligo27","spoligo28","spoligo29","spoligo30","spoligo31","spoligo32","spoligo33","spoligo34","spoligo35","spoligo36","spoligo37","spoligo38","spoligo39","spoligo40","spoligo41","spoligo42","spoligo43"];
  vntr15Header = ['VNTR_0580','VNTR_2996','VNTR_0802','VNTR_0960','VNTR_1644', 'VNTR_3192','VNTR_0424','VNTR_0577','VNTR_2165','VNTR_2401','VNTR_3690','VNTR_4156','VNTR_2163b','VNTR_1955','VNTR_4052'];


}
