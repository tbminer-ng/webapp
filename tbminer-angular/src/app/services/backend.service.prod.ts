import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  baseurl : string = '..'

  backendModel = [
    this.baseurl + "/api/predict/csv2json",
  ]
  backendSample: string = this.baseurl + "/api/data/sample";
  apiInputDescription: string = this.baseurl + "/api/front/inputdescription";

  apiOutputdescription: string = this.baseurl + "/api/front/outputdescription";

  apiOtherdescription: string = this.baseurl + "/api/front/otherdescription";

  apiContact: string = this.baseurl + "/api/front/contact";
}
