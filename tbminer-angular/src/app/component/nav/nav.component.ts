import {Component, OnInit} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {MatDialog} from "@angular/material/dialog";
import {NameCorrespondanceComponent} from "../side/name-correspondance/name-correspondance.component";
import {DialogTemplateComponent} from "../dialog-template/dialog-template.component";
import {DialogBoxModel} from "../../models/dialogbox.model";
import {HttpClient} from "@angular/common/http";
import {BackendService} from "../../services/backend.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit{

  input_descriptions! : DialogBoxModel[];
  output_descriptions! : DialogBoxModel[];
  other_description! : DialogBoxModel[];
  contact! : DialogBoxModel[];


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
              public dialog: MatDialog,
              private http: HttpClient,
              private backendService: BackendService,
) {

  }

  ngOnInit() {
    this.http.get<DialogBoxModel[]>(this.backendService.apiInputDescription)
      .subscribe(response => {
        //console.log(response)
        this.input_descriptions = response
      })
    this.http.get<DialogBoxModel[]>(this.backendService.apiOutputdescription)
      .subscribe(response => {
        //console.log(response)
        this.output_descriptions = response
      })
    this.http.get<DialogBoxModel[]>(this.backendService.apiOtherdescription)
      .subscribe(response => {
        //console.log(response)
        this.other_description = response
      })
    this.http.get<DialogBoxModel[]>(this.backendService.apiContact)
      .subscribe(response => {
        //console.log(response)
        this.contact = response
      })
  }




  openDialogTemplate(dialogContent: any) {
      this.dialog.open(DialogTemplateComponent,
        {
          data:{
        title: dialogContent.title,
        content : dialogContent.content
          },
            width: 'auto',
            height : 'auto'
          }
        )

  }
  openDialog3() {
    const dialogRef = this.dialog.open(NameCorrespondanceComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }


}

