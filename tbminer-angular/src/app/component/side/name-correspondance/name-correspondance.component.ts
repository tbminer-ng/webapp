import { Component } from '@angular/core';

@Component({
  selector: 'app-name-correspondance',
  templateUrl: './name-correspondance.component.html',
  styleUrls: ['./name-correspondance.component.css']
})
export class NameCorrespondanceComponent {
  vntrMiru = {'vntr_0154'	: 'MIRU_02',
    'vntr_0580'	: 'MIRU_04_|_ETRD',
    'vntr_0960' :	'MIRU_10',
    'vntr_1644'	: 'MIRU_16',
    'vntr_2059' :	'MIRU_20',
    'vntr_2531' :	'MIRU_23',
    'vntr_2687' :	'MIRU_24',
    'vntr_2996'	:	'MIRU_26',
    'vntr_3007'	:	'MIRU_27_|_QUBS',
    'vntr_3192'	:	'MIRU_31_|_ETRE',
    'vntr_4348'	:	'MIRU_39',
    'vntr_0802'	:	'MIRU_40',
    'vntr_0424'	:	'Mtub_04',
    'vntr_0577'	:	'ETRC',
    'vntr_1955'	:	'Mtub_21',
    'vntr_2163b'	:	'QUB_11b',
    'vntr_2165'	:	'ETRA',
    'vntr_2347'	:	'Mtub_29',
    'vntr_2401'	:	'Mtub_30',
    'vntr_2461'	:	'ETRB',
    'vntr_3171'	:	'Mtub_34',
    'vntr_3690'	:	'Mtub_39',
    'vntr_4156'	:	'QUB_4156',
    'vntr_4052'	:	'QUB_26',};
}
