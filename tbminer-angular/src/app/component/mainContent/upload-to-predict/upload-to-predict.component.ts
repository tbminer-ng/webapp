import {Component, OnInit, ViewChild} from '@angular/core';
import {Router,} from "@angular/router";
import {HttpClient, HttpParams} from "@angular/common/http";
import {CsvHeaderService} from "../../../services/csv-header.service";
import {TbminerModelService} from "../../../services/tbminer-model.service";
import {BackendService} from "../../../services/backend.service";
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder, FormControl} from "@angular/forms";
import {saveAs} from "file-saver";



@Component({
  selector: 'app-uploadtopredict',
  templateUrl: './upload-to-predict.component.html',
  styleUrls: ['./upload-to-predict.component.scss']
})

export class UploadToPredict implements OnInit {
  valid1 : FormControl = new FormControl('')
  valid2 : FormControl = new FormControl('')
  data: any;
  csvHeader: any;
  vntrHeader!: string[];
  vntr15Header!: string[];
  spHeader!: string[];
  option_sep: string;
  tbModel: TbminerModelService;
  backend: string[];
  file: any;
  headerRow: string[];
  selectHeader: any;
  fileNew: any;
  dataSource: any;
  key: string[] = [];
  newHeaderRow: string[] = [];
  newCsvRecordsArray: any;
  spoligoCheck: boolean;
  vntrCheck: boolean;
  vntr15Check: boolean;
  predictUrl: string;
  option_backend: string;
  csvRecordsArray!: string[];
  sendInProgress: any;
  selectedIndex: any;
  constructor(private router: Router,
              private http: HttpClient,
              private csvHeaderService: CsvHeaderService,
              private tbminerModel: TbminerModelService,
              private backendService: BackendService,
              private _formBuilder: FormBuilder,

  ) {
    this.sendInProgress = false;
    this.setState(this.valid1,true);
    this.setState(this.valid2,true);
    this.tbModel = tbminerModel;
    this.backend = backendService.backendModel;
    this.option_sep = tbminerModel.sep[0];
    this.headerRow = [];
    this.selectHeader = '';
    this.spoligoCheck = false;
    this.vntrCheck = false;
    this.vntr15Check = false;
    this.predictUrl = backendService.backendModel[0];
    this.option_backend = backendService.backendModel[0];
  }
  @ViewChild(MatPaginator) paginator!: MatPaginator;


  setState(control: FormControl, state: boolean) {
    if (state) {
      control.setErrors({ "required": true })
    } else {
      control.reset()
    }
  }
  ngOnInit(): void {
    this.csvHeader = this.csvHeaderService.csvHeader;
    this.vntrHeader = this.csvHeaderService.vntrHeader;
    this.vntr15Header = this.csvHeaderService.vntr15Header;
    this.spHeader = this.csvHeaderService.spHeader;
  }

  importFile($event: any): void {
    this.file = $event.target.files[0];
    this.setState(this.valid1,false)
  }

  regexRename(): boolean {


    for (let index in this.headerRow) {
      for (let keyMiru in this.tbminerModel.miruRegex) {
        if (this.headerRow[index].match(this.tbminerModel.miruRegex[keyMiru])){
          this.newHeaderRow[index] = this.headerRow[index].replace(this.tbminerModel.miruRegex[keyMiru], keyMiru)
        }
      }

      if (this.headerRow[index].match(this.tbminerModel.vntrRegex)){
        console.log(this.headerRow[index])
        this.newHeaderRow[index] = this.headerRow[index].replace(this.tbminerModel.vntrRegex, "VNTR_$1$2")
      }
      if (this.headerRow[index].match(this.tbminerModel.spRegex)){
        this.newHeaderRow[index] = this.headerRow[index].replace(this.tbminerModel.spRegex, "spoligo$1")
      }
    }
    console.log(this.newHeaderRow)
    this.vntrCheck = this.compareArrays(this.vntrHeader, this.newHeaderRow);
    this.vntr15Check = this.compareArrays(this.vntr15Header, this.newHeaderRow);
    this.spoligoCheck = this.compareArrays(this.spHeader, this.newHeaderRow);
    return (this.vntrCheck || this.vntr15Check || this.spoligoCheck)
  }

  preview(): void {
    let reader = new FileReader();
    reader.readAsText(this.file);
    reader.onload = () => {
      let csvData = reader.result;
      this.csvRecordsArray = (<string>csvData).split(/\r\n|\n/);
      this.headerRow = this.getHeaderArray(this.csvRecordsArray, this.option_sep);
      this.selectHeader = this.csvHeader.filter((x: any) => this.headerRow.includes(x));
      this.newHeaderRow = this.headerRow.slice();
      this.regexRename();
      this.vntrCheck = this.compareArrays(this.vntrHeader, this.newHeaderRow);
      this.vntr15Check = this.compareArrays(this.vntr15Header, this.newHeaderRow);
      this.spoligoCheck = this.compareArrays(this.spHeader, this.newHeaderRow);
      this.setState(this.valid2,!(this.vntrCheck || this.vntr15Check || this.spoligoCheck))
    }
  }
  
  compareArrays(a: string[], b: string[]): boolean {
    for (let aval of a) {
      if (!(b.includes(aval))) {
        return false
      }
    }
    return true
  }

  sendToTbminerAPI(input: any): void {
    console.log("launch predict");
    this.sendInProgress = true
    this.newCsvRecordsArray = this.modifyHeader(this.csvRecordsArray, this.newHeaderRow, this.option_sep);
    this.fileNew = new File([this.csvRecordsArray.join('\n')], 'file.csv', {type: "text/csv"});
    let params = new HttpParams()
      .set('model_input', input)
      .set('sep', this.option_sep)
      .set('timeout', 3000);
    console.log(params.toString())
    const options = {
      params: params,
    };
    let formData = new FormData();
    formData.append('file', this.fileNew);
    this.http.post<any>(this.option_backend, formData, options)
      .subscribe({next: train => {
          this.data = JSON.parse(train);
          this.sendInProgress = false;
          this.key = Object.keys(this.data[0]);
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
          console.log(this.dataSource);
  },
  error: error => {
    console.error('There was an error!', error);
    this.sendInProgress = false;
}
});
  }

  sendToTbminerAndDownload(input: any): void {
    console.log("launch predict");
    this.sendInProgress = true
    this.newCsvRecordsArray = this.modifyHeader(this.csvRecordsArray, this.newHeaderRow, this.option_sep);
    this.fileNew = new File([this.csvRecordsArray.join('\n')], 'file.csv', {type: "text/csv"});
    let params = new HttpParams()
      //.set('select_model', this.option_model)
      .set('model_input', input)
      .set('sep', this.option_sep)
      .set('timeout', 3000)
      .set('return_type', "csv" );

    // for csv response
    console.log(params.toString())
    let formData = new FormData();
    formData.append('file', this.fileNew);
    this.http.post<any>(this.option_backend, formData, {params : params, responseType: 'text' as 'json'})
      .subscribe({next: train => {
        console.log(train);
        var blob = new Blob([train], {type: 'text/csv' })
        saveAs(blob, "predict.csv",);
  },
  error: error => {
    console.error('There was an error!', error);
    this.sendInProgress = false;
}
      });
  }

  getHeaderArray(csvRecordsArr: any, sep: string) {
    let headers = (<string>csvRecordsArr[0]).split(sep);
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  modifyHeader(csvRecordsArr: any, newHeader: string[], sep: string) {
    csvRecordsArr[0] = newHeader.join(sep)
    return csvRecordsArr
  }


  isSticky(id: string, index: number) {
    let stickyColumns: string[] = [
      'ID',
    ]
    return !index;
  }

  isStickyEnd(id: string) {
    const pattern = /predict_.*/i;
    return (pattern.test(id.toString()));
  }

  DownloadSampleData() {
    this.http.get(this.backendService.backendSample, {responseType: 'text'})
      .subscribe(train => {
        console.log(train);

        var blob = new Blob([train], {type: 'text/csv' })
        saveAs(blob, "sample_vntr_sp.csv",);
      });
  }
}
