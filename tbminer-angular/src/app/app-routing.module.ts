

import {UploadToPredict} from "./component/mainContent/upload-to-predict/upload-to-predict.component";
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";


const routes: Routes = [

  {
    path: '', component: UploadToPredict
  },

]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
