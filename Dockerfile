# pull official base image
FROM python:3.9.5-slim-buster as train_model

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

#RUN apt-get update && apt-get install -y libgomp1

# install dependencies
RUN apt-get update && apt-get install -y libgomp1

RUN pip install --upgrade pip
COPY tbminer-fastapi/requirements.txt .
RUN pip install -r requirements.txt

RUN pip install python-multipart

COPY tbminer-fastapi/train_model.py /usr/src/app/train_model.py

COPY tbminer-fastapi/model_config/ /usr/src/app/model_config

COPY tbminer-fastapi/data/mtbc_clean.csv /usr/src/app/data/mtbc_clean.csv

RUN mkdir models

RUN python3 train_model.py

RUN ls -l models/





#####################
FROM node:18-alpine AS build

WORKDIR /app

COPY tbminer-angular/package*.json ./

RUN npm install --force

COPY tbminer-angular/ .

RUN npm run ng -- build --base-href ./


# pull official base image
FROM python:3.9.5-slim-buster

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

#RUN apt-get update && apt-get install -y libgomp1

# install dependencies
RUN apt-get update && apt-get install -y libgomp1

RUN pip install --upgrade pip
COPY tbminer-fastapi/requirements.txt .
RUN pip install -r requirements.txt

RUN pip install python-multipart pytest-cov

COPY tbminer-fastapi/ /usr/src/app

COPY --from=build /app/dist/tbminer-angular/ /usr/src/app/static

COPY --from=train_model /usr/src/app/models/ /usr/src/app/models


RUN pytest --cov -v test.py

EXPOSE 8000

VOLUME /usr/src/app/data/user/

CMD ["gunicorn", "main:app_root","--workers", "2","--worker-class" ,"uvicorn.workers.UvicornWorker" , "--bind", "0.0.0.0:8000", "--timeout", "3600"]

